ord_A = ord('A')
ord_J = ord('J')
j_index = ord_J - ord_A - 1

def l2i(letter):
    ord_letter = ord(letter)
    rv = ord_letter - ord_A
    if ord_letter >= ord_J:
        rv -= 1 
    return rv

def i2l(index):
    rv = ord_A + index
    if index >= j_index:
        rv += 1
    return chr(rv)

def h2c(human):
    if human == 'PASS':
        return ()
    return (l2i(human[0]), int(human[1:]) - 1)

def c2h(coord):
    if not coord:
        return 'PASS'
    return i2l(coord[0]) + str(coord[1] + 1)

def c2a(coord, size):
    if not coord:
        return size*size
    return coord[0]*size + coord[1]

def a2c(i, size):
    if i == size*size:
        return ()
    return divmod(i, size)

