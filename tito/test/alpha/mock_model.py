import numpy as np

class MockModel:
    def __init__(self, value):
        self.value = value
        self.predict_count = 0
        
    def predict(self, alpha_states):
        self.predict_count += 1
        size = alpha_states.shape[3]
        values = np.array([self.value], dtype=np.float32)
        ps_size = size*size + 1
        ps = np.full( shape=(ps_size,), dtype=np.float32, fill_value= 1/ps_size) 
                          
        return np.array([values]), np.array([ps])