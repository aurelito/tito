import unittest
from ...alpha.selfplay import SelfPlay

from .mock_model import MockModel

class Test(unittest.TestCase):
    
    def test_basic(self):
        sp = SelfPlay(model=MockModel(value=0.0), playout_count=3)
        
        sp.play_game()