import unittest

from ...alpha.mcts import MCTS
from ...game import Game

from .mock_model import MockModel

class Test(unittest.TestCase):
    
    def test_basic(self):
        game = Game(board_size=5)
        mock_model = MockModel(value=0.2)
        mcts = MCTS(model=mock_model)
        mcts.set_root(game.state)
        
        # root is expanded when set if it was not expanded already
        self.assertEqual(1, mock_model.predict_count)
        self.assertIn(mcts.root, mcts.state2node.values())
        self.assertEqual(0.2, mcts.root.model_value)
        self.assertEqual(1, mock_model.predict_count)
        self.assertEqual(0, sum(edge.n for edge in mcts.root.child_edges))

        self.assertIsNot(mcts.root, mcts.find_best_leaf())
        mcts.do_playouts(3)
        self.assertEqual(4, mock_model.predict_count)
        
        self.assertEqual(3, sum(edge.n for edge in mcts.root.child_edges))
        
        self.assertIsNotNone(mcts.choose_play_deterministic())
        self.assertIsNotNone(mcts.choose_play_multinomial())
        
        mcts.root_alpha_state()