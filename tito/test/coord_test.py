import unittest
from ..coord import l2i, i2l, c2h, h2c, c2a, a2c 

class Test(unittest.TestCase): 
    
    def test_letter_index(self):
        for index, letter in [
            (0, 'A'), 
            (1, 'B'),
            (8, 'J'), # No I
            (18, 'T')
        ]:
            actual_index = l2i(letter)
            self.assertEqual(
                index, 
                actual_index, 
                "Expected index %s for '%s' but got %s" % (index, letter, actual_index) 
            )
            
            actual_letter = i2l(index)
            self.assertEqual(
                letter, 
                actual_letter,
                "Expected letter '%s' for index %s but got '%s'" % (letter, index, actual_letter)
            )
            
    def test_human_coord(self):
        for coord, human in [
            ((0,0), 'A1'),
            ((0,4), 'A5'), 
            ((0,14),'A15'),
            ((1,0), 'B1'),
            ((1,4), 'B5'),
            ((8,0), 'J1'),
            ((), 'PASS')
        ]:
            actual_coord = h2c(human)
            self.assertEqual(
                actual_coord, 
                coord, 
                "Expected coords %s for '%s' but got %s" % (coord, human, actual_coord) 
            )
            
            actual_human = c2h(coord)
            self.assertEqual(
                actual_human, 
                human, 
                "Expected '%s' for coords '%s' but got '%s'" % (human, coord, actual_human) 
            )
            
    def test_alpha_coord(self):
        
        for c in [ (0,0), (0,4), (1,4), () ]:
            i = c2a(c, size=5)
            c2 = a2c(i, size=5)
            
            self.assertEqual(c, c2)
            
    
