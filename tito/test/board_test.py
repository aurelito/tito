import unittest

from tito.board import Board, LayeredBoard
from ..coord import h2c



class Test(unittest.TestCase):
    
    def test_size(self):
        b = Board(15)
        self.assertEqual(15, b.size)

    def test_get_set(self):
        b = Board(5)
        self.assertEqual(b.empty, b.get((1,1)))
        b.set((1,1), b.black)
        self.assertEqual(b.black, b.get((1,1)))
        b.set((1,1), b.empty)
        self.assertEqual(b.empty, b.get((1,1)))
        
    def test_constructor(self):
        black_stones = map(h2c, "E1 E5".split())
        white_stones = map(h2c, "A1 A5".split())
        b = Board(size=5, black=black_stones, white=white_stones)
        
        for d in black_stones:
            self.assertEqual(b.black, b.get(d), "Expected black stone at " + d)
            
        for d in white_stones:
            self.assertEqual(b.white, b.get(d), "Expected white stone at " + d)
            
        self.assertEqual(b.empty, b.get(h2c("C3")))
        
    def test_equal_hash(self):
        
        def check_equal_hash(o, o2):
            self.assertEqual(o, o2)
            self.assertEqual(hash(o), hash(o2))
            
        check_equal_hash(Board(5), Board(5))
        self.assertNotEqual(Board(5), Board(15))
        self.assertNotEqual(Board(5, white=[h2c("A1")]), Board(5))

        
    def test_bound_checking(self):
        b = Board(5)
        
        with self.assertRaises(IndexError):
            b.get((24,1))
        
        with self.assertRaises(IndexError):
            b.get((1,5))

    def test_neighbors(self):
        
        b = Board(5)
        self.assertSetEqual(
            {(1,3), (3,3), (2,4), (2,2)}, 
            set( b.neighbors((2,3)) )
        )
        self.assertSetEqual(
            {(0,1), (1,0)}, 
            set( b.neighbors((0,0)) )
        )
        
    def test_group(self):
        
        b = Board(
            size=5, 
            black={(1,1),(1,2),(2,3)},
            white={(1,3)}
        )
        
        self.assertSetEqual(set(), b.group((0,0))) # No group for empty space
        self.assertSetEqual({(1,1),(1,2)}, b.group((1,1))) # Only stones of the same color
        self.assertSetEqual({(1,3)}, b.group((1,3))) # Works with both colors

    def test_group_neighbors(self):
        
        b=Board(5)
        self.assertSetEqual({(1,0),(1,1),(0,2)}, b.group_neighbors({(0,0),(0,1)}))

    def test_reaches(self):
         
        b = Board(
            size=5,
            black={(1,1)},
            white={(1,2)}
        )
         
        self.assertEqual(b.empty, b.reaches(coord=(2,0), direction=(0,1)))
        self.assertEqual(b.white, b.reaches(coord=(1,4), direction=(0,-1)))
         
    def test_counting_color(self):
        b = Board(
            size=5,
            black={(1,1)},
            white={(2,2)}
        )
         
        self.assertEqual(b.black, b.counting_color((1,1)))
        self.assertEqual(b.empty, b.counting_color((1,2)))
        self.assertEqual(b.black, b.counting_color((1,3)))
        self.assertEqual(b.empty, b.counting_color((3,3)))
         
    def test_score(self):
        b = Board(
            size=5,
            black={(0,0),(0,1)},
            white={(0,2)} 
        )
         
        self.assertTupleEqual((10,7), b.score())
        
    def test_mask(self):
        b = Board(
            size=5,
            black={(1,1)},
            white={(1,0)}
        )
        
        mask = b.mask(color=b.white)
        self.assertEqual(0, mask[0,0]) # empty
        self.assertEqual(0, mask[1,1]) # black
        self.assertEqual(1, mask[1,0]) # white

    def test_duplicate(self):
        b = Board(
            size=5,
            black={(1,1)}
        )
        
        dup = b.duplicate()
        self.assertEqual(b, dup)
        self.assertIsNot(b, dup)
        
        b.set((0,0), b.white)
        self.assertEqual(dup.empty, dup.get((0,0)), "dup is aliased!")
         
    def test_str(self):
        b = Board(19)
        b.set(h2c("K10"), b.black)
        b.set(h2c("D4"), b.white)

        str(b)
        # No assert, just checking that it does not throw

class TestLayeredBoard(unittest.TestCase):
    
    def test_normal_usage(self):
        
        base = Board(size=5, black=map(h2c, "A1 A2".split()), white=map(h2c, "B1 B2".split()))
        topping = LayeredBoard(lower=base)
        
        topping.set(h2c("A3"), topping.white)
        topping.set(h2c("A2"), topping.white)
        topping.set(h2c("B2"), topping.empty)
        
        self.assertEqual(topping.get(h2c("A4")), topping.empty)
        self.assertEqual(topping.get(h2c("A3")), topping.white)
        self.assertEqual(topping.get(h2c("A2")), topping.white)
        self.assertEqual(topping.get(h2c("A1")), topping.black)
        self.assertEqual(topping.get(h2c("B2")), topping.empty)
        
        str(topping) # Just checking no exceptions are raised
