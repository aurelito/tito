import unittest

from ..board import Board
from ..coord import h2c
from ..game import GameState, Game, OccupiedPosition, SuicideIsForbidden, NoKo, FinishedGame,\
    GameException

import numpy as np
import io

class TestGameState(unittest.TestCase):
    
    def test_basic(self):
        
        s = GameState.empty( board_size=5 )
        s0 = s
        self.assertFalse( s.finished )
        self.assertIsNone(s.last_play)
        self.assertIsNone(s.parent)
        self.assertEqual(Board.black, s.turn)
        self.assertEqual(0, s.play_number)
        
        
        self.assertEqual( Board.empty, s.board.get(h2c("A2")) )
        s = s.play(h2c("A2"))
        self.assertEqual( Board.black, s.board.get(h2c("A2")) )
        self.assertEqual( Board.white, s.turn )
        self.assertEqual(h2c("A2"), s.last_play)
        self.assertIn(h2c("A1"), s.valid_plays())
        self.assertIs(s0, s.parent)
        self.assertEqual(1, s.play_number)

        s = s.play(h2c("A1"))
        self.assertIn((), s.valid_plays())
        self.assertEqual(Board.white, s.board.get(h2c("A1")))
        self.assertEqual( Board.black, s.turn )
        self.assertNotIn(h2c("A1"), s.valid_plays())
        
        self.assertNotIn(h2c("A1"), s.valid_plays())
        with self.assertRaises( OccupiedPosition ):
            s.play(h2c("A1"))


        s = s.play(h2c("B2")) \
            .play(h2c("B1")) \
            .play(h2c("C1")) # black captures
         
        self.assertEqual( Board.empty, s.board.get(h2c("A1")) ) # A1-B1 group is captured
        self.assertEqual( Board.empty, s.board.get(h2c("B1")) )
        self.assertIn(h2c("A1"), s.valid_plays()) # A1 is free again, so we can play :)
        
        s = s.play(h2c("A1")) # white plays again there after capture
        self.assertEqual( Board.white, s.board.get(h2c("A1")) )
        
        s_before_pass = s
        s = s.play(h2c("PASS"))
        self.assertEqual(h2c("PASS"), s.last_play)
        self.assertIs(s_before_pass.board, s.board)
        self.assertEqual(s_before_pass.play_number + 1, s.play_number)
        self.assertFalse(s.finished)
         
        self.assertNotIn(h2c("B1"), s.valid_plays())
        with self.assertRaises( SuicideIsForbidden ):
            s.play(h2c("B1"))
            
        # Make ko shape in the border
        s = s.play(h2c("A5")) \
            .play(h2c("D5")) \
            .play(h2c("B4")) \
            .play(h2c("C4")) \
            .play(h2c("C5")) \
            .play(h2c("B5")) 
        
        self.assertNotIn(h2c("C5"), s.valid_plays())
        with self.assertRaises( NoKo ):
            s.play(h2c("C5"))
        
        self.assertFalse(s.finished)
        s = s.play(())
        self.assertFalse(s.finished)
        s = s.play(())
        self.assertTrue(s.finished)
        
        with self.assertRaises( FinishedGame ):
            s.play(())
            
        s.alpha_state() # just check it does not crash
        
    def test_alpha_state(self):
        
        s = GameState.empty(board_size=5)
        as1 = s.alpha_state()
        self.assertEqual( (17,5,5), as1.shape )
        
        self.assertTrue( as1[16].all() ) # black to play
        self.assertTrue( np.array_equal( np.zeros((5,5), dtype=np.int ), as1[0] ) ) # no stones in board
        
        s = s.play((0,0))
        as2 = s.alpha_state()
        self.assertFalse( as2[16].any() ) # white to play
        self.assertFalse( as2[0].any() ) # no stones for current player
        
        # one stone for black. 
        self.assertEqual( 1, as2[8][0,0] ) 
        
        s = s.play((0,1))
        as3 = s.alpha_state()
        self.assertTrue( as3[16].all() ) # black to play
        
        self.assertTrue( as3[0].any() ) 
        self.assertTrue( as3[1].any() )
        self.assertFalse( as3[2].any() )
        self.assertTrue( as3[8].any() ) 
        self.assertFalse( as3[9].any() )
        
    def test_prune_parent(self):
        s0 = GameState.empty(board_size=5)
        s1 = s0.play((0,0))
        s0.play((0,1)) # generate orphan child
        
        self.assertTrue(s0.has_calculated_child((0,1)))
        s1.prune_parent()
        self.assertFalse(s0.has_calculated_child((0,1)))
        
        with self.assertRaises(GameException):
            s1.play((0,0)) # attempt to play again in the same spot, also generates orphan child
            
        self.assertTrue(s1.has_calculated_child((0,0)))
        s3 = s1.play((1,1))
        s3.prune_parent()
        self.assertFalse(s1.has_calculated_child((0,0)))
        
    def test_hash(self):
        
        g1 = GameState.empty(board_size=5)
        g2 = GameState.empty(board_size=5)
        
        self.assertNotEqual(g1,g2)
        self.assertNotEqual(hash(g1),hash(g2))
        
        
class TestGame(unittest.TestCase):
        
    def test_sgf(self):
        g = Game(board_size=5, komi=3.5)
        g.play(h2c("A1"))
        g.play(())
        g.play(h2c("A2"))
        
        buf = io.StringIO()
        g.save_sgf(file=buf, white_player="AA", black_player="BB", game_comment="GGCC")
        
        contents = buf.getvalue()
        
        self.assertIn("SZ[5]", contents)
        self.assertIn("KM[3.50]", contents)
        self.assertIn("PW[AA]", contents)
        self.assertIn("PB[BB]", contents)
        self.assertIn("GC[GGCC]", contents)
        self.assertIn("RU[Chinese]", contents)
        self.assertIn(";B[ae]", contents)
        self.assertIn(";W[]", contents)
        self.assertIn(";B[ad]", contents)
        
        buf2 = io.StringIO(contents)
        
        g2 = Game.load_sgf(buf2)
         
        self.assertEqual(g.state.board.size, g2.state.board.size)
        self.assertListEqual(list(s.board for s in g.state.states_iter()), list(s.board for s in g2.state.states_iter()))

