import numpy as np
from .coord import i2l, h2c

class BoardMixIn:

    empty = 0
    black = 1
    white = 2
    
    directions = { (1,0), (-1,0), (0,1), (0,-1) }
    
    def neighbors(self, coord):
        
        i = coord[0]
        j = coord[1]
        
        i1 = i - 1
        if i1 >= 0:
            yield (i1, j)
        
        i2 = i + 1
        if i2 < self.size:
            yield (i2, j)
        
        j1 = j - 1
        if j1 >= 0:
            yield (i, j1)
            
        j2 = j + 1
        if j2 < self.size:
            yield (i, j2)
            
    def group(self, coord):
        
        color = self.get(coord)
        if self.empty == color: # no stone, no group
            return set()
                
        candidates = {coord}
        rv = set()

        while candidates:
            c = candidates.pop()
            if color == self.get(c):
                rv.add(c)
                for c2 in self.neighbors(c):
                    if not c2 in rv:
                        candidates.add(c2)
        
        return rv

    def group_neighbors(self, group):
        rv = set()
        for d in group:
            for n in self.neighbors(d):
                if not n in group:
                    rv.add(n)
        return rv
    
    def reaches(self, coord, direction):
        try:
            while True:
                new_coord = (coord[0] + direction[0], coord[1] + direction[1]) 
                color = self.get(new_coord)
                if color != self.empty:
                    return color
                coord = new_coord
        except IndexError:
            return self.empty
        
    def counting_color(self, coord):
        "Color to be counted in position according to tromp-taylor scoring"
        c = self.get(coord)
        if c != self.empty:
            return c
        
        colors = set()
        
        for d in self.directions:
            c = self.reaches(coord, d)
            if c != self.empty:
                colors.add(c)
        
        return colors.pop() if len(colors) == 1 else self.empty
    
    def score(self):
        "Trump taylor score pair (black, white)"
        black = 0
        white = 0
        
        for i in range(self.size):
            for j in range(self.size):
                c = self.counting_color((i,j))
                if self.black == c:
                    black += 1
                elif self.white == c:
                    white += 1
                    
        return black, white

    def __str__(self):
        
        def letters():
            rv = "   "
            for i in range(self.size):
                rv += i2l(i) + " "
                
            return rv
        
        rv = letters() + "\n"

        for j in range(self.size -1, -1, -1):
            rv += "%2d " % (j+1)
            for i in range(self.size):
                rv += self.char_for_coord((i,j)) 
                rv += " "
            rv += "%d\n" % (j+1)
        
        rv += letters()
        return rv
    

class Board(BoardMixIn):
    
    chars = {
        BoardMixIn.empty: ".",
        BoardMixIn.white: "o",
        BoardMixIn.black: "x"
    }
    
    def __init__(self, size=19, matrix=None, black=[], white=[]):
        
        self.matrix = np.zeros(shape=(size,size), dtype=np.int) if matrix is None else matrix
        
        for d in black:
            self.set(d, self.black)
            
        for d in white:
            self.set(d, self.white)
            
        self.hoshis = None
            
            
    def __eq__(self, other):
        return np.array_equal(self.matrix, other.matrix)
    
    def __hash__(self):
        return hash(bytes( self.matrix.data ))
        
    @property
    def size(self):
        return self.matrix.shape[0]
    
    def get(self, coord):
        return self.matrix[coord]
    
    def set(self, coord, color):
        self.matrix[coord] = color
        return self
        
    def mask(self, color):
        return 1 * (self.matrix == color)
    
    def duplicate(self):
        return type(self)(matrix = self.matrix.copy()) 
    
    def free_coords(self):
        return map(tuple, np.transpose(np.where(self.matrix ==0)))
        
    def char_for_coord(self, coord):
        
        if not self.hoshis:
            if self.size == 9:
                self.hoshis = {h2c(h) for h in "C7 G7 E5 C3 G3".split()}
            elif self.size == 13:
                self.hoshis = {h2c(h) for h in "C10 K10 G7 D4 K4".split()}
            elif self.size == 19:
                self.hoshis = {h2c(h) for h in "D16 K16 Q16 D10 K10 Q10 D4 K4 Q4".split()}
            elif self.size % 2 == 1:
                middle = self.size // 2
                self.hoshis = {(middle, middle)}
            else:
                self.hoshis = set()    
        
        color = self.get(coord)
        if self.empty == color and coord in self.hoshis:
            return "+"
        return self.chars[color]
    
class LayeredBoard(BoardMixIn):
    
    changed_chars = {
        BoardMixIn.empty: ":",
        BoardMixIn.white: "O",
        BoardMixIn.black: "X",
    }
    
    def __init__(self, lower):
        super().__init__()
        self.lower = lower
        self.layer = dict()
        
    @property
    def size(self):
        return self.lower.size
    
    def char_for_coord(self, coord):
        color = self.get(coord)
        return self.changed_chars[color] if color != self.lower.get(coord) else self.lower.char_for_coord(coord)
    
    def get(self, coord):
        return self.layer.get(coord, self.lower.get(coord))
    
    def set(self, coord, color):
        default = self.lower.get(coord)
        
        if color == default:
            del self.layer[coord]
        else:
            self.layer[coord] = color
    
        return self