import sys
from argparse import ArgumentParser
from tito.game import Game
from tito.board import Board, LayeredBoard
from .coord import h2c

def parser():
    rv = ArgumentParser(description='Play against another human being')
    rv.add_argument('-s', '--size', default=19, type=int, help='Board size (default: %(default)s)')
    rv.add_argument('-k', '--komi', default=5.5, type=float, help='Komi (default: %(default)s)')
    
    
    return rv

color_name = {
    Board.empty: 'empty',
    Board.black: 'black',
    Board.white: 'white'
}

def save_if_requested(cmd, game):
    if not cmd.upper().startswith("SAVE "):
        return False
    
    fname = cmd.split()[1]
    with open(fname, "w") as f:
        game.save_sgf(file=f)
        
    print("Saved", fname)
    return True

def score(game):
    print("*" * 50)
    print("Scoring")
    human_marks = LayeredBoard(lower=game.state.board)
    while True:
        print("Scoring. Komi %s" % game.komi)
        print(human_marks)
        black_score, white_score = human_marks.score()
        print("Black:", black_score, "White:", white_score + game.komi)
        try:
            cmd = input("Mark dead group (save fname to save): ")
            if save_if_requested(cmd=cmd, game=game):
                continue
            marked_group = game.state.board.group(h2c(cmd.upper()))
            for c in marked_group:
                orig_color = human_marks.get(c)
                human_marks.set(c, Board.black if Board.white == orig_color else Board.white)
        
        except Exception as e:
            print(e)


        
class HumanCoordinator:
    def __init__(self, game, black_agent, white_agent):
        self.game = game
        self.next_agent = black_agent
        self.other_agent = white_agent
    
    def play(self):
        while not self.game.finished:
            print(self.game.state.board)
            try:
                self.game.play(self.next_agent.play())
                self.next_agent, self.other_agent = self.other_agent, self.next_agent
            except Exception as e:
                print(e)
                
        print("=" * 50)
        
        score(self.game)

class HumanAgent:
    def __init__(self, game):
        self.game = game
        
    def play(self):
        while True:
            print("%s to play. Komi %s" % (color_name[self.game.turn], self.game.komi))
            cmd = input("Enter play (pass to pass, save fname to save): ")
            if save_if_requested(cmd=cmd, game=self.game):
                continue
            try:
                return h2c(cmd.upper())
            except Exception as e:
                print(e)


def play(size, komi, **kwargs):  # @UnusedVariable
    
    game = Game(board_size=size, komi=komi)
    coordinator = HumanCoordinator(
        game = game, 
        black_agent = HumanAgent( game = game ),
        white_agent = HumanAgent( game = game )
    )
    
    coordinator.play()
        

def main(argv):
    args = parser().parse_args(argv[1:])
    play(**vars(args))
    

if __name__ == '__main__':
    main(sys.argv)