from tito.board import Board

import numpy as np
import collections
from sgfmill.sgf import Sgf_game

class GameException(Exception): pass

class OccupiedPosition(GameException): pass

class SuicideIsForbidden(GameException): pass

class NoKo(GameException): pass

class FinishedGame(GameException): pass

class SgfException(GameException): pass

class GameState(object):

    @classmethod
    def empty(cls, board_size=19):
        rv = cls(parent=None, board = Board(size=board_size), last_play=None)
        
        return rv
    
    def __init__(self, parent, board, last_play, play_number=0):
        self.parent = parent
        self.play_number = play_number
        self.boards = set()
        self.board = board
        self.last_play = last_play
        self.finished = False
        self.passed = False
        self.expanded_children = False
        self.children = {}
        self.children_exceptions = {}
        
        if parent:
            self.turn = Board.white if parent.turn == Board.black else Board.black
            for b in parent.boards:
                self.boards.add(b)
        else: 
            self.turn = Board.black
            
        self.boards.add(board)
        
    def play(self, coord):
        
        if self.finished:
            raise FinishedGame("Cannot play in a finished game")
        
        self.child_for(coord)

        if coord in self.children:
            return self.children[coord]
        else:
            raise self.children_exceptions[coord]
            
    def valid_plays(self):
        self.expand_children()
        return self.children.keys()
    
    def alpha_state(self):
        "Game state as described by alphago papers"
        rv = np.zeros( (17, self.board.size, self.board.size), dtype=np.int )
        black_to_play = self.turn == self.board.black
        
        current_state = self
        for i in range(8):
            if black_to_play:
                black_index = i
                white_index = i+8
            else:
                white_index = i
                black_index = i+8
            
            b = current_state.board
            rv[black_index] = b.mask(b.black)
            rv[white_index] = b.mask(b.white)
            
            current_state = current_state.parent
            if current_state is None:
                break
        
        if black_to_play:
            rv[16].fill(1) # last l all 1s
        return rv
    
    def state_stack(self):
        rv = collections.deque()
        
        s = self
        while s:
            rv.append(s)
            s = s.parent
            
        return rv
    
    def states_iter(self):
        stack = self.state_stack()
        while stack:
            yield stack.pop()
    
    def prune_parent(self):
        self.parent.children = {self.last_play: self}
        self.parent.children_exceptions = {}
        self.parent.expanded_children = False
        
        return self

    def expand_children(self):
        if self.expanded_children:
            return # work has been already made
        
        self.expanded_children = True
        
        if self.finished:
            return # no need for expansion in finished game
        
        self.child_for(()) # child for pass
        
        for i,j in self.board.free_coords():
            self.child_for((i,j))
            
    def has_calculated_child(self, coord):
        return coord in self.children or coord in self.children_exceptions
    
    def child_for(self, coord):
        
        if self.finished:
            return # cannot play in finished games!
        
        if self.has_calculated_child(coord):
            return # already calculated
        
        if not coord: #pass
            next_state = type(self)(parent=self, board=self.board, last_play=coord, play_number = self.play_number+1)
            if self.passed:
                next_state.finished = True
            else:
                next_state.passed = True
            self.children[()] = next_state
            return
        
        if self.board.empty != self.board.get(coord):
            self.children_exceptions[coord] = OccupiedPosition("Position %s is occupied" % str(coord))
            return
        
        board = self.board.duplicate()
        board.set(coord, self.turn)
        
        liberties = lambda group: {d for d in board.group_neighbors(group) if board.empty == board.get(d)}
        
        current_group = board.group(coord)
        
        neighbor_groups = {
            frozenset(board.group(d)) for d in board.neighbors(coord) if not d in current_group 
        }
        
        # do captures
        captures = set()
        for ng in neighbor_groups: #always oposite color, would be inside current group otherwise
            if not ng: # handle dame
                continue
            
            neighbor_liberties = liberties( ng )
            
            if not neighbor_liberties:
                for d in ng:
                    board.set(d, board.empty)
                    captures.add(d)

        # suicide is forbidden
        if not liberties(current_group):
            self.children_exceptions[coord] = SuicideIsForbidden("Cannot play at %s. Suicide is forbidden")
            return
        
        # no board repetition
        if board in self.boards:
            self.children_exceptions[coord] = NoKo("Cannot play at %s. Board position has appeared before.")
            return
        
        self.children[coord]=type(self)(parent=self, board=board, last_play=coord, play_number=self.play_number+1)
        
class Game:
    
    @classmethod
    def load_sgf(cls, file):
        
        sgf_game = Sgf_game.from_string(file.read())
        
        rv = cls(
            board_size = sgf_game.get_size(),
            komi = sgf_game.get_komi()
        )
        
        for n in sgf_game.get_main_sequence():
            color, coord = n.get_move()
            
            if not color is None:
                if coord is None:
                    rv.play(())
                else:
                    rv.play((coord[1],coord[0]))
                
        return rv
        

    
    def __init__(self, board_size=19, komi=7.5):
        self.komi = komi
        self.state = GameState.empty(board_size=board_size)
        
    @property
    def turn(self):
        return self.state.turn
        
    @property
    def finished(self):
        return self.state.finished
    
    @property
    def passed(self):
        return self.state.passed
        
    def play(self, coord):
        self.state = self.state.play(coord)
        self.state.prune_parent() # no need to keep analysis for parent
        
    def save_sgf(self, file, white_player="WHITE", black_player="BLACK", game_comment="", rules="Chinese"):
        def entry(k,v):
            print( "%s[%s]" % (k,v), file=file)
            
        def sgf_coord(coord):
            if not coord: # pass
                return ""
            ord_a = ord('a')
            return "%s%s" % ( chr( ord_a + coord[0]), chr( ord_a + (self.state.board.size - 1 - coord[1])) )
         
        file.write("(;")
        entry("GM", "1") # go
        entry("FF", "4") # version 4
        entry("CA", "UTF-8")
        entry("AP", "tito")
        entry("RU", rules)
        entry("SZ", str(self.state.board.size))
        entry("KM", "%.2f" % self.komi)
        entry("PW", white_player)
        entry("PB", black_player)
        entry("GC", game_comment)
        
        black_to_play = True
        
        it = self.state.states_iter()
        next(it) # skip initial state, no play generated it
        
        for s in it:
            file.write(';')
            entry(
                "B" if black_to_play else "W",
                sgf_coord(s.last_play)
            )
            
            black_to_play = not black_to_play
        
        file.write(")")
