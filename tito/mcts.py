from argparse import ArgumentParser

import sys
import time
import random

from .coord import c2h
from .game import Game, GameException
from .board import Board
from .human import HumanCoordinator, HumanAgent
from .alpha.model import load as load_model
from .alpha.mcts import MCTS

def parser():
    rv = ArgumentParser(description='Play against another human being')
    rv.add_argument('-s', '--size', default=9, type=int, help='Board size (default: %(default)s)')
    rv.add_argument('-k', '--komi', default=5.5, type=float, help='Komi (default: %(default)s)')
    rv.add_argument('-w', '--play-white', default=False, action='store_true', help='Play with white (with black if not set)')
    rv.add_argument('-p', '--playout-count', default=1000, type=int, help='Number of playouts for each move. (detault: %(default)s)')
    rv.add_argument('-m', '--model', help="Model file. If not set it will use the Naive MCTS method")
    
    return rv

class AlphaAgent:
    def __init__(self, game, playout_count, model_fname=None):
        self.game = game
        self.playout_count = playout_count
        self.mcts = MCTS(model=load_model(model_fname))
        
    def play(self):
        self.mcts.set_root(self.game.state)
        
        t0 = time.time()
        
        print("Tito is running %s playouts" % self.playout_count)

        self.mcts.do_playouts(self.playout_count)
        rv = self.mcts.choose_play_deterministic()

        print("It took %.2f seconds" % (time.time() - t0))
        print( "Computer played", c2h(rv))
        
        return rv 

class NaiveMCTSAgent:
    def __init__(self, game, playout_count):
        self.game = game
        self.playout_count = playout_count
    def play(self):
        
        if self.game.passed :
            print("Human passes, computer passes. Lets score!")
            return () # pass if passed
        
        print("Computer thinking. %s playouts. Please wait." % self.playout_count)
            
        t0 = time.time()
        playouts = {}
        max_play_count = 2 * self.game.state.board.size * self.game.state.board.size
        
        count = 0
        
        def play_random(state):
            free_coords = list(state.board.free_coords())
            max_ = len(free_coords)
            b = state.board
            color = state.turn
            for _ in range(5):
                try:
                    index = random.randint(0, max_)
                    if index == max_:
                        coord = ()
                    else:
                        coord = free_coords[index] 
                    if coord:
                        if all(color == b.get(c) for c in b.neighbors(coord)): # avoid playing in eyes
                            break
                    return coord, state.play(coord)
                except GameException:
                    pass
                
            return (), state.play(()) # cannot find move, just pass
    
        for _ in range(self.playout_count):
            count += 1
            initial_state = self.game.state
            coord, state = play_random(initial_state)
            while state.play_number < max_play_count:
                if state.finished:
                    break
                _, state = play_random(state)
                          
            black_score, white_score = state.board.score()
            white_score += self.game.komi
            
            playout_counts = playouts.get(coord, [0,0])
            if black_score > white_score:
                playout_counts[0] += 1
            else:
                playout_counts[1] += 1
            playouts[coord] = playout_counts
            
        pc_index = 0 if Board.black == self.game.turn else 1
        best_p = 0
        best_coord = ()
        best_count = 0
        
        def calc_p(pc):
            if pc[0] + pc[1] == 0:
                return 0
            else:
                return pc[pc_index] / (pc[0]+pc[1])
            
        for c, p, cc in ((coord, calc_p(pc), pc[0] + pc[1]) for (coord,pc) in playouts.items()):
            if p > best_p:
                best_p = p
                best_coord = c
                best_count = cc
                
        print( "It took %.2f seconds" % (time.time() - t0))
        print( "Computer played", c2h(best_coord), "p=", best_p, "count=", best_count )
        
        return best_coord
            

def play_game(size, komi, play_white, playout_count, model):
    
    game = Game(board_size=size, komi=komi)
    human_agent = HumanAgent( game = game )
    computer_agent = \
        AlphaAgent(game=game, playout_count=playout_count, model_fname=model) if model \
        else NaiveMCTSAgent( game=game, playout_count=playout_count)
    coordinator = HumanCoordinator(
        game = game, 
        black_agent = computer_agent if play_white else human_agent,
        white_agent = human_agent if play_white else computer_agent
    )
    
    coordinator.play()
    
def main(argv):
    args = parser().parse_args(argv[1:])
    play_game(**vars(args))
    

if __name__ == '__main__':
    main(sys.argv)

