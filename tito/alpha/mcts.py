import numpy as np
import math
from ..coord import c2a, a2c
from ..alpha.model import policy_out_dim

class MCTSException(Exception):
    pass

class InvalidStateColor(Exception):
    pass

class NodeState:
    def __init__(self, ps, boards):
        self.ps = ps
        self.boards = boards

class Node:
    
    def __init__(self, parent_edge, game_state):
        self.parent_edge = parent_edge
        self.game_state = game_state
        self.child_edges = []
        self.model_value = None
        
    @property
    def finished(self):
        return self.game_state.finished
    
    @property
    def expanded(self):
        return not self.model_value is None
    
    def closure(self):
        yield self
        for e in self.child_edges:
            for n in e.child.closure():
                yield n
        
        
class Edge:
    
    @classmethod
    def incomplete(cls, parent, prior):
        return cls(parent=parent, prior=prior, child=None)
    
    def is_incomplete(self):
        return self.child is None
    
    def __init__(self, coord, parent, child, prior):
        self.coord = coord
        self.parent = parent
        self.child = child
        self.n = 0
        self.w = 0
        self.q = 0
        self.p = prior
    
    def mark_visit(self, value):
        self.n += 1
        self.w += value
        self.q = self.w/self.n

        
class MCTS:
    
    def __init__(self, model, c_puct=3.0, root_noise_fun=None, epsilon=0.25):
        self.model = model
        self.color = None
        self.root = None
        self.state2node = {}
        self.c_puct = c_puct
        self.root_noise_fun = root_noise_fun
        self.epsilon = epsilon

    def set_root(self, game_state):
        if self.color is None:
            self.color = game_state.turn
        
        if self.color != game_state.turn:
            raise InvalidStateColor("Expected color %s in state %s but got %s" % self.color, game_state, game_state.color)
        
        if game_state in self.state2node:
            self.root = self.state2node[game_state]
            self.root.parent_edge = None # previous analysis becomes unreachable
            self.state2node = {n.game_state: n for n in self.root.closure()} # only keep descendants of new root
        else:
            self.state2node = {}
            self.root = self.build_and_register_node(parent_edge=None, game_state=game_state)
            
        if not self.root.expanded:
            self.expand_leaf(self.root)
            
        if self.root_noise_fun:
            noise = self.root_noise_fun(len(self.root.child_edges))
            for e in self.root.child_edges:
                e.p = (1-self.epsilon) * e.p + self.epsilon * noise.pop()
            
    def build_and_register_node(self, **kwargs):
        node = Node(**kwargs)
        self.state2node[node.game_state] = node
        return node
        
    def find_best_leaf(self, node=None):
        if node is None:
            node = self.root

        if not node.expanded or node.finished:
            return node # leaf found!
        
        max_q_u = -math.inf
        max_e = None

        for e in node.child_edges:
            k_u = e.p * self.c_puct / (e.n + 1)
            sum_n = sum( e2.n for e2 in e.child.child_edges )
            u = k_u * math.sqrt(sum_n)

            q_u = e.q + u
            if q_u > max_q_u:
                max_q_u = q_u
                max_e = e
                
        return self.find_best_leaf(max_e.child)
    
    def expand_leaf(self, node):
        
        inputToModel = np.array( [node.game_state.alpha_state()] )
        value_array, ps_array = self.model.predict(inputToModel)
        
        def invalid(ary):
            return np.any(np.isnan(ary)) or np.any(np.isinf(ary))
        
        if invalid(inputToModel) or invalid(value_array) or invalid(ps_array):
            print( "inputToModel", repr(inputToModel))
            print( "value_array", repr(value_array))
            print( "ps_array", repr(ps_array))
            raise Exception("INVALID!!!")
        
        value = value_array[0]
        if self.color != node.game_state.turn:
            value = -value
        node.model_value = value
            
        ps = ps_array[0]
        
        if not node.finished: # no children for finished positions
            # Normalize ps
            valid_plays = list(node.game_state.valid_plays())         
            mask = np.ones(shape=ps.shape, dtype=np.bool)
            for coord in valid_plays:
                mask[c2a(coord, node.game_state.board.size)] = False
            ps[mask] = 0
            ps = ps / np.sum(ps)
            
            for coord in valid_plays:
                edge = Edge(coord=coord, parent=node, child=None, prior=ps[c2a(coord, node.game_state.board.size)])
                child = self.build_and_register_node(parent_edge=edge, game_state=node.game_state.play(coord))
                edge.child = child
                node.child_edges.append(edge)
                
        # mark expansion
        
        e = node.parent_edge
        
        while e:
            e.mark_visit(node.model_value)
            e = e.parent.parent_edge
            
    def do_playout(self):
        leaf = self.find_best_leaf()
        self.expand_leaf(leaf)
    
    def do_playouts(self, n):
        for _ in range(n):
            self.do_playout()
            
    def choose_play_deterministic(self):
        rv = None
        max_n = 0
        
        for edge in self.root.child_edges:
            if edge.n > max_n:
                max_n = edge.n
                rv = edge.coord
                
        return rv
    
    def chosen_proportions(self, dtype=np.float32):
        board_size = self.root.game_state.board.size
        counts = np.zeros(dtype=dtype, shape=(policy_out_dim(board_size),))
        
        for e in self.root.child_edges:
            counts[c2a(e.coord, board_size)] = e.n
        rv = counts / np.sum(counts)
        return rv  
        
    
    def choose_play_multinomial(self):
        board_size = self.root.game_state.board.size
        # float64 to avoid numpy bug. See: https://github.com/numpy/numpy/issues/8317
        action_idx = np.random.multinomial(1, self.chosen_proportions(dtype=np.float64)) 
        return a2c( np.where(action_idx==1)[0][0], board_size )
    
    def root_alpha_state(self):
        
        return NodeState(
            ps = self.chosen_proportions(),
            boards = self.root.game_state.alpha_state()
        )

