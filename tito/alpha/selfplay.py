import numpy as np
from ..game import Game
from ..board import Board
from .mcts import MCTS

class SelfPlay:
    def __init__(self, model, alpha=0.3, board_size=9, komi=7.5, playout_count=800, multinomial_steps=30):
        self.model = model
        self.alpha = alpha
        self.board_size = board_size
        self.komi = komi
        self.playout_count = playout_count
        self.multinomial_steps = multinomial_steps
        
        
    def noise(self, length):
        return list( np.random.dirichlet([self.alpha] * length) )
    
    def play_game(self):
        game = Game(board_size=self.board_size, komi=self.komi)
        
        mctss = {
            Board.black: MCTS(model=self.model, root_noise_fun=self.noise),
            Board.white: MCTS(model=self.model, root_noise_fun=self.noise)
        }
        
        states = []
        
        while not game.finished:
            mcts = mctss[game.turn]
            mcts.set_root(game.state)
            mcts.do_playouts(self.playout_count)
            
            states.append(mcts.root_alpha_state())
            
            coord = mcts.choose_play_multinomial() \
                if game.state.play_number < self.multinomial_steps \
                else mcts.choose_play_deterministic()
            
            game.play(coord)
            
        states.append(mcts.root_alpha_state())
            
        return game, states
        