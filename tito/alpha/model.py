from keras.layers import Input, Conv2D, BatchNormalization, LeakyReLU, add, Flatten, Dense
from keras.models import Model, load_model
from keras.optimizers import SGD
from keras import regularizers

import tensorflow as tf

def save(model, filepath, *args, **kwargs):
    model.save(filepath=filepath)
    
def load(filepath):
    return load_model(
        filepath=filepath
    )
    
def input_shape(board_size):
    return (17,board_size,board_size)

def policy_out_dim(board_size):
    return board_size * board_size + 1

class ModelBuilder:
    """
Builds a model according to the description in the "Neural Network Architecture" of the paper
"Mastering the Game of Go without Human Knowledge" (https://deepmind.com/documents/119/agz_unformatted_nature.pdf)

Code based upon model.py in https://github.com/AppliedDataSciencePartners/DeepReinforcementLearning
"""
    
    
    def __init__(self, 
        board_size=9, 
        filter_count=256, 
        kernel_size=(3,3), 
        reg_const = 0.0001, 
        hidden_layer_count=19,
        momentum = 0.9,
        learning_rate = 0.2,
    ):
        self.board_size = board_size
        self.input_shape = input_shape(board_size)
        self.policy_out_dim = policy_out_dim(board_size)
        self.filter_count = filter_count
        self.kernel_size = kernel_size
        self.reg_const = reg_const
        self.hidden_layer_count = hidden_layer_count
        self.momentum = momentum
        self.learning_rate = learning_rate
        
    def conv_layer(self, x):
    
        x = Conv2D(
            filters = self.filter_count,
            kernel_size = self.kernel_size,
            kernel_regularizer = regularizers.l2(self.reg_const),
            data_format="channels_first",
            padding = 'same',
            use_bias=False,
            activation='linear',
        )(x)

        x = BatchNormalization(axis=1)(x)
        x = LeakyReLU()(x)
    
        return x
    
    def residual_layer(self, x0):
        x = self.conv_layer(x0)

        x = Conv2D(
            filters = self.filter_count,
            kernel_size = self.kernel_size,
            kernel_regularizer = regularizers.l2(self.reg_const),
            data_format="channels_first",
            padding = 'same',
            use_bias=False,
            activation='linear',
        )(x)

        x = BatchNormalization(axis=1)(x)
        x = add([x0, x])
        x = LeakyReLU()(x)

        return x
    
    def policy_head(self, x):

        x = Conv2D(
            filters = 2, 
            kernel_size = (1,1), 
            data_format="channels_first", 
            padding = 'same', 
            use_bias=False, 
            activation='linear', 
            kernel_regularizer = regularizers.l2(self.reg_const)
        )(x)

        x = BatchNormalization(axis=1)(x)
        x = LeakyReLU()(x)

        x = Flatten()(x)

        x = Dense(
            self.policy_out_dim, 
            use_bias=False, 
            activation='softmax', 
            kernel_regularizer=regularizers.l2(self.reg_const), 
            name = 'policy_head'
        )(x)

        return x
    
    def value_head(self, x):

        x = Conv2D(
            filters = 1,
            kernel_size = (1,1),
            data_format="channels_first", 
            padding = 'same', 
            use_bias=False, 
            activation='linear', 
            kernel_regularizer = regularizers.l2(self.reg_const)
        )(x)

        x = BatchNormalization(axis=1)(x)
        x = LeakyReLU()(x)

        x = Flatten()(x)

        x = Dense(
            256, 
            use_bias=False,
            activation='linear', 
            kernel_regularizer=regularizers.l2(self.reg_const)
        )(x)

        x = LeakyReLU()(x)

        x = Dense(
            1, 
            use_bias=False, 
            activation='tanh', 
            kernel_regularizer=regularizers.l2(self.reg_const), 
            name = 'value_head'
        )(x)

        return x

        
    def __call__(self):
        
        main_input = Input(
            shape = self.input_shape,
            name = 'main_input'
        )
        
        x = self.conv_layer(main_input)
        
        for _ in range(self.hidden_layer_count):
            x = self.residual_layer(x)
            
        vh = self.value_head(x)
        ph = self.policy_head(x)
        
        model = Model(inputs=[main_input], outputs=[vh, ph])
        model.compile(
            loss=dict(
                value_head='mean_squared_error', 
                policy_head='categorical_crossentropy'
            ),
            optimizer=SGD(
                lr=self.learning_rate, 
                momentum = self.momentum),    
            loss_weights=dict(
                value_head=0.5, 
                policy_head=0.5
            )    
        )

        return model
