from .selfplay import SelfPlay

import numpy as np
import random

from argparse import ArgumentParser
import sys
from pathlib import Path
from .model import load, save


class Train:
    def __init__(self, model, batch_size=32, playout_count=800, komi=7.5, board_size=9):
        self.model=model
        self.playout_count=playout_count
        self.komi=komi
        self.board_size=board_size
        self.batch_size=batch_size
        
    def train_batch(self):
        sp = SelfPlay(
            model=self.model, 
            board_size=self.board_size, 
            komi=self.komi, 
            playout_count=self.playout_count
        )
        
        games = []
        
        for i in range(self.batch_size):
            print("Playing game", i)
            games.append(sp.play_game())
            
        indexes = [ random.randrange(len(states)) for (_, states) in games]
        
        def training_state(i):
            return games[i][1][indexes[i]].boards
        
        training_states = np.array( [training_state(i) for i in range(self.batch_size)], dtype=np.float32 )
        
        def policy(i):
            return games[i][1][indexes[i]].ps
        
        def value(i):
            game = games[i][0]
            black_points, white_points = game.state.board.score()
            white_points += self.komi
            value = -1**indexes[i]
            if white_points > black_points:
                value *= -1 
            return np.array( [value], dtype=np.float32 )
        
        training_targets = dict(
            value_head = np.array( [value(i) for i in range(self.batch_size)] ),
            policy_head = np.array( [policy(i) for i in range(self.batch_size)] )
        )
        
        def invalid(ary):
            return np.any(np.isnan(ary)) or np.any(np.isinf(ary))
        
        if invalid(training_states) or invalid(training_targets['value_head']) or invalid(training_targets['policy_head']):
            print( "training_states", repr(training_states))
            print( "training_targets['value_head']", repr(training_targets['value_head']))
            print( "training_targets['policy_head']", repr(training_targets['policy_head']))
            raise Exception("INVALID!!!")

        
        self.model.fit(
            training_states, 
            training_targets, 
            epochs=1, 
            verbose=1, 
            validation_split = 0, 
            batch_size = self.batch_size
        )
    
def parser():
    rv = ArgumentParser(description='Runs forever training a model')
    rv.add_argument('-s', '--size', default=9, type=int, help='Board size (default: %(default)s)')
    rv.add_argument('-k', '--komi', default=5.5, type=float, help='Komi (default: %(default)s)')
    rv.add_argument('-p', '--playout-count', default=400, type=int, help='Number of playouts for each move. (detault: %(default)s)')
    rv.add_argument('-b', '--batch-size', default=32, type=int, help='Number of games to accumulate before refining model. (detault: %(default)s)')
    rv.add_argument('initial_model', help='Model to base our work upon')
    rv.add_argument('output_dir', help='Where new models will be written')
    
    return rv

def train(size, komi, playout_count, batch_size, initial_model, output_dir):
    output_path = Path(output_dir)
    output_path.mkdir()
    
    model = load(initial_model)
        
    trainer = Train(
        model=model, 
        batch_size=batch_size, 
        playout_count=playout_count, 
        komi=komi, 
        board_size=size
    )
    
    era = 0
    while True:
        print( "Starting era", era )
        trainer.train_batch()
        output =str(output_path / ("%d.h5" % era))
        save(
            model = model, 
            filepath = output
        )
        print("Saved", output)
        era += 1
        
def main(argv):
    args = parser().parse_args(argv[1:])
    train(**vars(args))

if __name__ == '__main__':
    main(sys.argv)