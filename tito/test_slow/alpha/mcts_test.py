import unittest
from . import gms
from ...alpha.mcts import MCTS
from ...game import Game

class Test(unittest.TestCase):
    
    def setUp(self):
        self.model = gms.model() 
        
    def test_exercise_mcts_with_tensorflow_model(self):
        game = Game(board_size=9)
        mcts = MCTS(model=self.model)
        mcts.set_root(game.state)
        
        self.assertEqual(0, sum(edge.n for edge in mcts.root.child_edges)) # Root node expanded
        self.assertIsNot(mcts.root, mcts.find_best_leaf())
        mcts.do_playouts(2)
        
        self.assertEqual(2, sum(edge.n for edge in mcts.root.child_edges))
        
        self.assertIsNotNone(mcts.choose_play_deterministic())