from ...alpha.model import ModelBuilder

instance = None


def model():
    global instance
    
    if not instance:
        
        builder = ModelBuilder(
            board_size=9,
            hidden_layer_count=5
        )
        
        instance = builder()
    
    return instance