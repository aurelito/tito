import unittest

from . import gms

from ...alpha.model import save, load, policy_out_dim
from ...game import Game

import os
import numpy as np

class Test(unittest.TestCase):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def setUp(self):
        self.model = gms.model()
            
    def test_save_and_load(self):

        cur_dir = os.path.abspath(os.path.dirname(__file__))
        fname = os.path.join(cur_dir, "save_and_load.h5")
        
        save(self.model, fname)
        load(fname)
        
    def test_predict(self):
         
        g = Game(board_size=9)
        
        # Any game :)
        
        g.play((0,0))
        g.play((1,1))
        g.play((2,2))
        g.play((3,3))
        
        value_array, ps_array = self.model.predict(np.array([g.state.alpha_state()]))
        self.assertEqual((1,), value_array[0].shape)
        self.assertEqual((82,), ps_array[0].shape)
        
    def test_fit(self):
         
        g = Game(board_size=9)
         
        # Any game :)
         
        g.play((0,0))
        g.play((1,1))
        g.play((2,2))
        g.play((3,3))
         
        states = np.array([g.state.alpha_state()])
        silly_targets = dict(
            value_head = np.array([np.array([1], dtype = np.float32)]),
            policy_head = np.array([np.array([0.5] * policy_out_dim(9), dtype = np.float32)])
        )

        _ = self.model.fit(
            states,
            silly_targets,
            epochs = 1,
            validation_split=0,
            batch_size=32
        )
 
        